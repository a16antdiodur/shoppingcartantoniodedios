package org.iesam.antoniodedios.test;

import static org.junit.Assert.*;

import org.iesam.antoniodedios.Product;
import org.iesam.antoniodedios.ProductNotFoundException;
import org.iesam.antoniodedios.ShoppingCartAntoniodeDios;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ShoppingCartTest {

	private ShoppingCartAntoniodeDios _bookCart;
    private Product _defaultBook;

    /**
     * Sets up the test fixture.
     * Called before every test case method.
     */
    @Before
    public void prepararTest() {

        _bookCart = new ShoppingCartAntoniodeDios();

        _defaultBook = new Product("Extreme Programming", 23.95);
        _bookCart.addItem(_defaultBook);
    }

    /**
     * Tears down the test fixture.
     * Called after every test case method.
     */
    @After
    public void limpiarTest() {
        _bookCart = null;
    }
    
    @Test
	public void compruebaVacio() {
    	_bookCart.empty();
		assertTrue(_bookCart.isEmpty() == true);
	}
    
    @Test
    public void anadirProducto() {
    	Product producte = new Product("Producte 2", 10.00);
    	_bookCart.addItem(producte);
    	
    
    	assertTrue(_bookCart.getBalance() == 33.95);
    	assertTrue(_bookCart.getItemCount() == 2);
    }
    
    @Test(expected = ProductNotFoundException.class)
    public void productoNoExiste() throws ProductNotFoundException {
    	Product prod = new Product ("Producte 3", 3.40);
    	
    	_bookCart.removeItem(prod);
    	fail();
    }
    
    @Test
    public void quitarProducto() throws ProductNotFoundException {
    	_bookCart.removeItem(_defaultBook);
    	assertTrue(_bookCart.getBalance() == 0);
    }

}
